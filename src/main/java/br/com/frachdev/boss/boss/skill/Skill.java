package br.com.frachdev.boss.boss.skill;

import br.com.frachdev.boss.boss.Boss;

public interface Skill {

    String name();
    int cooldown();
    int percent();

    void execute(Boss boss);

}