package br.com.frachdev.boss.boss;

import br.com.frachdev.boss.Base;
import br.com.frachdev.boss.boss.skill.Skill;
import com.google.common.collect.Lists;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class BossSenttings {

    private EntityType type;

    private int maxHealth = 20;
    private ItemStack egg = null;
    private String display = "Undefined";

    private List<String> commands = Lists.newArrayList();
    private List<ItemStack> itens = Lists.newArrayList();

    private List<Skill> skills = Lists.newArrayList();

    public BossSenttings(String boss){
        this.type = Base.config.getEntityType("Boss." + boss + ".type");
        this.maxHealth = Base.config.getInt("Boss." + boss + ".hp");
        this.egg = Base.config.getItemStack("Boss." + boss + ".egg");
        this.display = Base.config.getString("Boss." + boss + ".name");

        this.commands = Base.config.getStringList("Boss." + boss + ".rewards.commands");
        this.itens = Base.config.getItemStackSection("Boss." + boss + ".rewards.itens");

        for (String s : Base.config.getStringList("Boss." + boss + ".skills")) {
            Skill skill = Base.skills.stream().filter(sk -> sk.name().equals(s)).findFirst().orElse(null);
            if(skill != null){
                skills.add(skill);
            }
        }

    }

    public EntityType getType() {
        return type;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public List<String> getCommands() {
        return commands;
    }

    public List<ItemStack> getItens() {
        return itens;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public String getDisplay() {
        return display;
    }

    public ItemStack getEgg() {
        return egg;
    }

}