package br.com.frachdev.boss.boss.skill;

import br.com.frachdev.boss.boss.Boss;

public class JumpSkill implements Skill {

    @Override
    public String name() {
        return "Jump";
    }

    @Override
    public int cooldown() {
        return 30;
    }

    @Override
    public int percent() {
        return 5;
    }

    @Override
    public void execute(Boss boss) {
        System.out.println("Skill executada");
    }

}