package br.com.frachdev.boss.boss;

import br.com.frachdev.boss.Base;
import br.com.frachdev.boss.SentinelTrait;
import br.com.frachdev.boss.boss.skill.Skill;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Boss {

    private NPC npc;
    private Player owner;
    private String name;
    private BossSenttings bossSenttings;

    private Random random = new Random();
    private BukkitTask runnable;

    public Boss(String name, Player player){
        this.owner = player;
        this.name = name;
        bossSenttings = new BossSenttings(name);

        npc = CitizensAPI.getNPCRegistry().createNPC(bossSenttings.getType(), getBossSenttings().getDisplay());
        npc.addTrait(SentinelTrait.class);
        npc.spawn(player.getLocation());

        SentinelTrait trait = npc.getTrait(SentinelTrait.class);
        trait.owner = player;

        trait.respawnTime = -1;

        trait.health = getBossSenttings().getMaxHealth();
        trait.attackRate = 10;

        trait.targetingHelper.addTarget(player.getUniqueId());

        trait.getLivingEntity().setMaxHealth(bossSenttings.getMaxHealth());
        trait.getLivingEntity().setHealth(bossSenttings.getMaxHealth());

        Base.bosses.add(this);

        runnable = new BukkitRunnable(){
            @Override
            public void run() {
                int rand = random.nextInt(100);

                List<Skill> skills = bossSenttings.getSkills().stream().filter(skill -> skill.percent() <= rand).collect(Collectors.toList());

                skills.forEach(skill -> skill.execute(Boss.this));

            }
        }.runTaskTimer(Base.instance, 20L, 20L);

    }

    public String getName() {
        return name;
    }

    public Player getOwner() {
        return owner;
    }

    public NPC getNpc() {
        return npc;
    }

    public BossSenttings getBossSenttings() {
        return bossSenttings;
    }

    public void death(){
        getBossSenttings().getCommands().forEach(command ->{
            command = command.replace("%player%", getOwner().getName());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
        });
        getBossSenttings().getItens().forEach(getOwner().getInventory()::addItem);
        getOwner().sendMessage("§6[Boss] §aVocê matou o boss §f" + getName());
        if(runnable != null) runnable.cancel();
    }

}